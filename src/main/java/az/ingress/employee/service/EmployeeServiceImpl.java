package az.ingress.employee.service;

import az.ingress.employee.dto.EmployeeDto;
import az.ingress.employee.mapper.EmployeeMapper;
import az.ingress.employee.model.Employee;
import az.ingress.employee.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService{
    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;
    @Override
    public EmployeeDto saveEmployee(EmployeeDto employeeDto) {
        Employee employee = employeeMapper.mapToEmployee(employeeDto);
        Employee savedEmployee = employeeRepository.save(employee);
        return employeeMapper.mapToEmployeeDto(savedEmployee);
    }

    @Override
    public EmployeeDto updateEmployee(Integer employeeId, EmployeeDto employeeDto) {
        employeeRepository.findById(employeeId).orElseThrow(() -> new RuntimeException("Not found Employee with id = " + employeeId));
        Employee employee = employeeMapper.mapToEmployee(employeeDto);
        employee.setId(employeeId);
        Employee savedEmployee = employeeRepository.save(employee);
        return employeeMapper.mapToEmployeeDto(savedEmployee);
    }

    @Override
    public List<EmployeeDto> getAllEmployees() {
        return employeeMapper.mapToEmployeeDtoList(employeeRepository.findAll());
    }

    @Override
    public EmployeeDto getEmployeeById(Integer employeeId) {
        Employee employeeDb = employeeRepository.findById(employeeId).orElseThrow(() -> new RuntimeException("Not found Employee with id = " + employeeId));
        return employeeMapper.mapToEmployeeDto(employeeDb);
    }

    @Override
    public void deleteEmployee(Integer employeeId) {
        employeeRepository.findById(employeeId).orElseThrow(() -> new RuntimeException("Not found Employee with id = " + employeeId));
        employeeRepository.deleteById(employeeId);
    }
}

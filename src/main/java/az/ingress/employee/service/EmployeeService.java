package az.ingress.employee.service;

import az.ingress.employee.dto.EmployeeDto;

import java.util.List;


public interface EmployeeService {
    EmployeeDto saveEmployee(EmployeeDto employeeDto);
    EmployeeDto updateEmployee(Integer employeeId, EmployeeDto employeeDto);
    List<EmployeeDto> getAllEmployees();
    EmployeeDto getEmployeeById(Integer employeeId);
    void deleteEmployee(Integer employeeId);



}

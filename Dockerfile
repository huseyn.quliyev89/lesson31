FROM openjdk:17
COPY build/libs/*.jar /app/
WORKDIR /app/
RUN mv /app/*.jar /app/jira.jar
ENTRYPOINT ["java"]
CMD ["-jar","/app/jira.jar"]
